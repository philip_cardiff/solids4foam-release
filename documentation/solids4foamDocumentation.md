# solids4foam Documentation

A video tutorial covering much of this content can be found at <https://youtu.be/D3AyykFAW3c>, which was delivered as part of the 3rd UCL OpenFOAM Workshop, 24th February 2021, online. 

- ### [Notes on this Documentation](notesOnDocumentation)

- ### [Installation](installation)

- ### [Overview](overview)

- ### [My First Solid Case](tutorials/my_first_solid_case)
    - #### Overview
    - #### Theory
    - #### Running the Case
    - #### Structure

- ### [My First Fluid Case](tutorials/my_first_fluid_case)
    - #### Overview
    - #### Theory
    - #### Running the Case
    - #### Structure

- ### [My First Fluid-Solid Interaction Case](tutorials/my_first_fluid_solid_interaction_case)
    - #### Overview
    - #### Theory
    - #### Running the Case
    - #### Structure

- ### [Running More Solid Cases](tutorials/more_solid_cases)

- ### [References & Acknowledgement](tutorials/references)
